<?php

namespace Twenti\LazyLoad\Model;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_LazyLoad
 */

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{

	const LAZY_LOAD_ENABLED = 'catalog/lazy_load/enabled';

	/**
	 * @var ScopeConfigInterface
	 */
	protected $scopeConfig;

	/**
	 * Constructor
	 *
	 * @param  ScopeConfigInterface  $scopeConfig
	 */
	public function __construct(
		ScopeConfigInterface $scopeConfig
	)
	{
		$this->scopeConfig = $scopeConfig;
	}

	/**
	 * Determines if lazy load is enabled
	 *
	 * @return boolean
	 */
	public function isLazyLoadEnabled() : bool
	{
		return (bool) $this->scopeConfig->isSetFlag( self::LAZY_LOAD_ENABLED );
	}

}
