# Magento 2 Lazy Load Module #

---


[Lazy Load](https://github.com/verlok/lazyload) for Magento 2.


## Compatibility ##
  * Magento Community Edition or Enterprise Edition 2.3.x


## Installing via composer ##
  * Open command line
  * Using command "cd" navigate to your Magento 2 root directory
  * Run commands:
    * ```composer require twenti/m2-module-lazy-load```
    * ```php bin/magento setup:upgrade```
    * ```php bin/magento setup:di:compile```
    * ```php bin/magento setup:static-content:deploy```


## Support ##
For any issues, please open a support ticket here:
[issue tracker](https://bitbucket.org/twenti/m2-module-lazy-load/issues).


## Need More Features ? ##
Please contact us to get a quote
https://twenti.io/contact


## Other extensions by Twenti ##
  * [Store Label](https://bitbucket.org/twenti/m2-module-store-label).
