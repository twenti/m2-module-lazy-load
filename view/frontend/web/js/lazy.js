
/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_LazyLoad
 */

if ( 'IntersectionObserver' in window ) {
    require( [
        'Twenti_LazyLoad/js/lazyload'
    ], function( LazyLoad ) {
        const lazy = new LazyLoad( {
            elements_selector: ".lazy"
        } );
    } );
}
else {
    require( [
        'Twenti_LazyLoad/js/intersection-observer',
        'Twenti_LazyLoad/js/lazyload'
    ], function( _, LazyLoad ) {
        const lazy = new LazyLoad( {
            elements_selector: ".lazy"
        } );
    } );
}
