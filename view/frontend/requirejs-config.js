
/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_LazyLoad
 */

var config = {
    map: {
        '*' : {
            'Twenti_LazyLoad/js/intersection-observer': 'Twenti_LazyLoad/js/lazyload/intersection-observer.amd',
            'Twenti_LazyLoad/js/lazyload': 'Twenti_LazyLoad/js/lazyload/lazyload.amd'
        }
    },
	deps: [
        'Twenti_LazyLoad/js/lazy',
 	]
};
