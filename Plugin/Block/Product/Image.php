<?php

namespace Twenti\LazyLoad\Plugin\Block\Product;

/**
 * @author    Filippo Bovo
 * @copyright Copyright (c) 2018-2019 Twenti (https://twenti.io)
 * @package   Twenti_LazyLoad
 */

use Twenti\LazyLoad\Model\Config;
use Magento\Catalog\Block\Product\Image as Subject;

class Image
{

	/**
	 * @var Config
	 */
	private $config;

	/**
	 * Constructor
	 *
	 * @param  \Twenti\LazyLoad\Model\Config  $config
	 */
	public function __construct(
		Config $config
	)
	{
		$this->config = $config;
	}

	/**
	 * @param  Subject  $subject
	 * @param  string   $template
	 * @return array
	 */
	public function beforeSetTemplate( Subject $subject, string $template ) : array
	{
		if ( $this->config->isLazyLoadEnabled() ) {
			// Override template
			$template = str_replace( 'Magento_Catalog', 'Twenti_LazyLoad', $template );
		}

		return [ $template ];
	}

}
